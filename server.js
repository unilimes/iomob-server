var net = require('net');

var taxiRoutesJson = require('./data/taxi-routes.json');
var userRoutesJson = require('./data/user-routes.json');

var netServer = net.createServer(function (c) {
    console.log('client connected');

    c.on('end', function () {
        console.log('client disconnected');
    });

    c.on('error', function (err) {
        console.log('Error: ', err);
    });

    c.on('data', function (data) {
        // data = JSON.parse(data);
        console.log(data);
        userRoutesJson.user_id = data.user_id;
        c.write(JSON.stringify(userRoutesJson));
    });

    var now = Date.now();
    var index = 0;

    setInterval(function () {
        var time = (Date.now() - now) / 1000;

        if (taxiRoutesJson[index]) {
            var routeTime = taxiRoutesJson[index].time;

            if (routeTime <= time) {
                if (~taxiRoutesJson[index].name.indexOf('taxi')) {
                    taxiRoutesJson[index].type = 'trip';
                } else if (~taxiRoutesJson[index].name.indexOf('psgr')) {
                    taxiRoutesJson[index].type = 'psgr';
                }
                c.write(JSON.stringify(taxiRoutesJson[index++]));
                // c.pipe(c);
                // index++
            }
        }
    }, 50);
});

netServer.listen(23456, function () {
    console.log('server running at 23456');
});