var app = require('express')();
var server = require('http').createServer(app);
var io = require('socket.io')(server, {pingTimeout: 90000});
var port = process.env.PORT || 3005;
// var net = require('net');
var zmq = require('zeromq');
var sock = zmq.socket('sub');
var requester = zmq.socket('req');

var adress = 'tcp://5.9.65.109:23456';
// var adress = 'tcp://127.0.0.1:3210';

var routesAdress = 'tcp://5.9.65.109:5543';
// var routesAdress = 'tcp://127.0.0.1:5543';

function getRandomId() {
    return +(Math.random()*1000000).toFixed(0);
}

function checkUserInTransport(data, user_id) {
    if (!data.client_ids) return false;
    return data.client_ids.some( function(id) { return id === user_id });
}

sock.connect(adress);
sock.subscribe('sim ');
console.log('Node server connected to ' + adress);

requester.on("message", function(reply) {
    var data = JSON.parse(reply.toString());
    var user_id = data.user_id;
    delete data.user_id;
    delete data['5'];
    console.log("Received reply:", data);
    io.to(user_id).emit('userRoutesList', data);
});
requester.on("error", function(err) {
    console.log('ZMQ Routes ERROR: ', err);
});
requester.connect(routesAdress);

// var taxiRoutesJson = require('./data/taxi-routes.json');
// var userRoutesJson = require('./data/demo_input4.json');
// var userRoutesJson = require('./data/user-routes.json');

/*
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.get('/userRoutes', function (req, res) {
    res.status(200).json({
        status: true,
        message: 'User Routes',
        data: userRoutesJson
    })
});
*/

io.sockets.setMaxListeners(0); // TODO (09.07.18): Determine the required number of listeners

io.on('connection', function(client) {
    var client_id = getRandomId();
    // console.log('Connected clients: ', Object.keys(io.sockets.sockets));

    sock.on('message', function(message) {
        // console.log(message.toString());
        if (message.toString().slice(0, 3) === 'sim') {
            var data = JSON.parse(message.toString().slice(4));
            // if (~data.name.indexOf('bus') || ~data.name.indexOf('metro')) console.log(data);
            if (~data.name.indexOf('psgr#' + client_id) || checkUserInTransport(data, client_id)) data.self = true;
            client.emit('trips', data)
        }
    });

    client.on('getUserRoutes', function(coords) {
        requester.send(JSON.stringify({
            user_id: client.id,
            client_id: client_id,
            origin: coords.origin,
            dest: coords.destination
        }));
    });

    client.on('onRouteSelect', function(selectedRoute) {
        console.log('User', client_id, client.id, 'choose route', selectedRoute);
        requester.send(JSON.stringify({
            user_id: client.id,
            client_id: client_id,
            result: selectedRoute.toString()
        }));
    });

    client.on('disconnect', function() {
        console.log('client disconnected', client.id);
        // console.log('Connected clients: ', Object.keys(io.sockets.sockets));
    });
    client.on('error', function(err) {
        console.log('Client ERROR: ', err);
    });
});

io.on('error', function(err) {
    console.log('Socket ERROR: ', err);
});

server.listen(port, function() {
    console.log('http running at ' + port);
});
