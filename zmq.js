var zmq = require('zeromq');
var sock = zmq.socket('pub');
var responder = zmq.socket('rep');

responder.on('message', function(request) {
    console.log("Received request:", request.toString());
    setTimeout(function() {
        responder.send("World");
    }, 1000);
});

responder.bind('tcp://127.0.0.1:5543', function(err) {
    if (err) {
        console.log(err);
    } else {
        console.log("Listening on 5543…");
    }
});

var taxiRoutesJson = require('./data/taxi-routes.json');
// var userRoutesJson = require('./data/user-routes.json');

sock.bindSync('tcp://127.0.0.1:3210');
console.log('Publisher bound to port 3210');

var now = Date.now();
var index = 0;

setInterval(function () {
    var timestamp = Date.now();
    var time = (timestamp - now) / 1000;

    if (taxiRoutesJson[index]) {
        var routeTime = taxiRoutesJson[index].time;

        if (routeTime <= time) {
            if (~taxiRoutesJson[index].name.indexOf('taxi')) {
                taxiRoutesJson[index].type = 'trip';
            } else if (~taxiRoutesJson[index].name.indexOf('psgr')) {
                taxiRoutesJson[index].type = 'psgr';
            }
            sock.send('sim ' + JSON.stringify(taxiRoutesJson[index++]));
            // index++
        }
    }
}, 50);

/*
setInterval(function(){
    sock.send(['kitty cats', Date.now()]);
}, 1000);*/
